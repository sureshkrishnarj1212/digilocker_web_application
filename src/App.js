import * as React from 'react';
import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import TextField from '@mui/material/TextField';
import Box from '@mui/material/Box';
import axios from 'axios'
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import { createTheme, ThemeProvider } from '@mui/material/styles';

const theme = createTheme();

export default function App() {
  
  const handleSubmit = (event) => {
    event.preventDefault();
    const data = new FormData(event.currentTarget);
    console.log({name: data.get('name')});
  };

  const authorization = async () => {

    await axios
      .get("https://digilocker.meripehchaan.gov.in/public/oauth2/1/authorize?response_type=code&client_id=8E224E46&redirect_uri=https://addressadmin.matex.co.in/fetchVerifiedDocs&state=pdfDetail&verified_mobile=9943326975")
      .then((response) => {
        console.log(response,"name")
      })
      .catch((error) => {
        console.log(error,"_____________")
      });

  };

  return (
    <ThemeProvider theme={theme}>
      <Container component="main" maxWidth="xs">
        <CssBaseline />
        <Box
          sx={{
            marginTop: 8,
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
          }}
        >
          <Avatar sx={{ m: 1, bgcolor: 'secondary.main' }}>
            {/* <LockOutlinedIcon /> */}
          </Avatar>
          <Typography component="h1" variant="h5">
            Matrix
          </Typography>
          <Box component="form" onSubmit={handleSubmit} noValidate sx={{ mt: 1 }}>
            <TextField
              margin="normal"
              required
              fullWidth
              id="name"
              label="Name"
              name="name"
              autoComplete="name"
              autoFocus
              color='secondary'
            />
            <label style={{marginTop:'50px'}}>
              Aadhar Card Number * :
            </label>
            <Button
              fullWidth
              variant="outlined"
              color='secondary'
              sx={{ mt: 1, mb: 1 }}
              onClick={authorization}
            >
              Upload From Digilocker
            </Button>
            <label>
              PAN Card Number * :
            </label>
            <Button
              fullWidth
              variant="outlined"
              color='secondary'
              sx={{ mt: 1, mb: 1 }}
            >
              Upload From Digilocker
            </Button>
            <Button
              type="submit"
              fullWidth
              color='secondary'
              variant="contained"
              sx={{ mt: 3, mb: 2 }}
            >
              Submit
            </Button>
          </Box>
        </Box>
      </Container>
    </ThemeProvider>
  );
}

// import logo from './logo.svg';
// import './App.css';
// import { FormControl, FormHelperText, Input, InputLabel } from '@mui/material';

// function App() {
//   return (
//     <div className="App">
//       {/* <header className="App-header">
//         <form>
//           <label>
//             Name:
//             <input type="text" name="name" />
//           </label>
//           <label>
//             Aadhar Number:
//             <input type="text" name="name" />
//           </label>
//           <input type="submit" value="Submit" />
//         </form>
//       </header> */}
//       <FormControl>
//         <InputLabel htmlFor="my-input">Email address</InputLabel>
//         <Input id="my-input" aria-describedby="my-helper-text" />
//         <FormHelperText id="my-helper-text">We'll never share your email.</FormHelperText>
//       </FormControl>
//     </div>
//   );
// }

// export default App;

